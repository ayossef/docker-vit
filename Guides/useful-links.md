# Useful Links

## Using Docker Commit
Creating an image from a running container 
https://docs.docker.com/engine/reference/commandline/commit/

## Persist DB 
How to use volumes 
https://docs.docker.com/engine/reference/commandline/commit/


## Mongo Docker Hub
This url contains the documentation of mongo docker hub image.
It also contains a simple docker-compose file 
https://hub.docker.com/_/mongo


## Gitlab Docker Image Building and Pushing
This url contains a simple guide with sample codes for building and pushing docker images
to gitlab registery. 
https://docs.gitlab.com/ee/user/packages/container_registry/build_and_push_images.html

## Deploy NGINX into a docker image
This url contains the guide used for creating a docker image that contains a web application hosted on 
an Nginx web server
https://docs.nginx.com/nginx/admin-guide/installing-nginx/installing-nginx-docker/

Nginx docker hub image
https://hub.docker.com/_/nginx

