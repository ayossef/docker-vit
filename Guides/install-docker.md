# Installing Docker on Ubuntu

We have two options:
- Use the existing servers with apt
  ```bash
  apt install docker
  # it is installing an older version of docker
  ```

- Adding official docker servers to apt and then use these new servers for installing docker.

## Installing using docker servers

We need to run the following:

### 1. Adding the docker servers to the apt

```bash
sudo apt install apt-transport-https ca-certificates curl software-properties-common
# installing the required tools for verification
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
# Adding ther server keys
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
# Adding the servers to the available list
```

### 2. Use apt for installing the most recent version of docker
By running these commands

```bash
sudo apt update
sudo apt install docker-ce
```

### 3. Optional - Making docker accessible to the curent user
```bash
sudo usermod -aG docker ${USER}
```