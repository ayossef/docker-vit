# Basic K8s Commands

## Installing Minikube

### 1. Installing Qemu-KVM
Qemu is a virtualization software needed to run the Minikube. 
Control-plane is VM

### 2. Install Minikube
Downloading, making it executable and moving it to the bin folder

### 3. Optinal - Install kubectl
Kubectl is the main command line client for K8s
We have other alternatives and we have a built-in version of kubectl inside minikube


## Basic Commands

### Minikube Related Commands

```bash
minikube status
minikube start
minikube stop
```

```bash
minikube kubectl
```

```bash
minikube dashboard
# starts the UI management dashborad
```


### Kubectl Related Commands

```bash
kubectl version
```
Kubectl get, is used to get a resource
```bash
# kubectl get
kubectl get nodes
kubectl get pods
kubectl get services
kubectl get configmap
kubectl get secret
kubectl get namespaces
```

## Service related commands

```bash
kubectl get services
```

```bash
kubectl create service clusterip --port 9000
# create an internal service
```

```bash
kubectl expose deployment my-deployment --type=NodePort --name=node-service --port 80
# creates a node port service to be exposing it outside the cluster
```