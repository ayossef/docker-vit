#!/bin/bash

sudo apt update
sudo apt install snapd
sudo snap install microk8s --classic
sudo adduser --disabled-password --gecos "" mk8s
sudo usermod -aG microk8s mk8s
sudo usermod -aG sudo mk8s
sudo su - mk8s 
cd
sudo chown -f -R $USER ~/.kube
microk8s start
microk8s enable dns
microk8s enable hostpath-storage
microk8s status --wait-ready
